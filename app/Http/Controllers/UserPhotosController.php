<?php

namespace App\Http\Controllers;

use App\UserPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class UserPhotosController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $photos = UserPhoto::where('user_id', auth()->user()->id)->get();

        return view('userphotos.index', compact('photos'));
    }

    public function create()
    {
        return view('userphotos.upload');
    }

    public function store(Request $request)
    {
        $path = $request->file('photo')->store('photos');

        $request->user()->photos()->create([
            'path' => $path,
            'hash' => hash('md5', Uuid::uuid1()->toString())
        ]);

        return redirect()->route('userphoto.index');
    }

    public function destroy(Request $request, UserPhoto $photo)
    {
        /*
         * Necessário validar a requisição
         * Como teste apenas verifico se o user_id da photo a ser
         * excluida pertence ao usuario da requisição
         * ---
         * Alertas e avisos omitidos
         */

        if ($photo->user_id == $request->user()->id) {
            Storage::delete($photo->path);
            $photo->delete();
        }

        return redirect()->route('userphoto.index');
    }
}
