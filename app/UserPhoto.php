<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    protected $fillable = [
        'user_id',
        'hash',
        'path'
    ];

    public function getRouteKeyName()
    {
        return 'hash';
    }
}
