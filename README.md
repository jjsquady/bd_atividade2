# Atividade 02

### BlueDental

#### Escopo:

Construa um cadastro onde o usuário possa enviar uma foto utilizando uma API Rest.

#### Resolução

Projeto em Laravel 5 (5.4.X), inclui TDD e a resolução do escopo.

Utilizado o *auth scafolding* do laravel para o cadastro de usuários,
adicionado a possibilidade do usuário fazer upload,
visualizar e excluir fotos.

#### Requisitos

* Requisitos do [laravel 5.4](https://laravel.com/docs/5.4#server-requirements)
* Composer
* PHPUnit (para os testes)

## Instalação

```
git clone https://bitbucket.org/jjsquady/bd_atividade2.git
```

##### Na pasta do projeto (via terminal) faça:

```
$ composer install
$ cp .env.example .env
$ php artisan migrate:refresh
$ php artisan db:seed
$ php artisan serve
```

#### Crie um link simbólico para o diretório de upload:

`$ php artisan storage:link`

Abra o endereço `http://localhost:8000` no navegador.

**Login:** test@test.com

**Senha:** 123456

### Testes

```
phpunit tests/
```

## API REST

Acessível via `http://localhost:8000/api/`

- **GET** `api/photos` : lista as fotos enviadas

- **POST** `api/photos` : envia uma foto
    - **BODY**
    - parâmetros: *photo*
    - tipo: *arquivo*

- **DELETE** `api/photos/:hash:` : exclui uma foto
