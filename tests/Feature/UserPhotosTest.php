<?php

namespace Tests\Feature;

use App\User;
use App\UserPhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;

class UserPhotosTest extends TestCase
{
    public function createUser()
    {
        return factory(User::class)->create();
    }

    public function login()
    {
        $this->actingAs($this->createUser());
    }

    /** @test */
    public function it_can_get_a_photo_path()
    {
        $user = $this->createUser();

        $user->photos()->save(UserPhoto::create([
            'user_id' => 1,
            'hash' => hash('md5', 'uuid'),
            'path' => 'photo.jpg'
        ]));

        $this->assertEquals('photo.jpg', $user->photos->first()->path);
    }

    /** @test */
    public function it_can_upload_photo()
    {
        $this->login();

        $response = $this->get('userphotos/upload');

        $response->assertViewIs('userphotos.upload');
        $response->assertSee('name="photo"');
    }

    /** @test */
    public function it_can_store_photo()
    {
        Session::start();

        Storage::fake('public');

        $this->login();

        $response = $this->post('userphotos', [
            '_token' => csrf_token(),
            'photo' => UploadedFile::fake()->create('photo.jpg')
        ]);

        Storage::disk('public')->assertExists(UserPhoto::first()->path);

        $response->assertRedirect('userphotos');
    }

    /** @test */
    public function it_view_photos()
    {
        $this->login();

        $response = $this->get('userphotos');

        $response->assertViewIs('userphotos.index');
    }

    /** @test */
    public function it_delete_a_photo()
    {
        Session::start();

        Storage::fake('public');

        $this->login();

        $this->post('userphotos', [
            '_token' => csrf_token(),
            'photo' => UploadedFile::fake()->create('photo.jpg')
        ]);

        $response = $this->get('userphotos/'. UserPhoto::first()->hash .'/delete');

        $response->assertRedirect('userphotos');
    }
}
