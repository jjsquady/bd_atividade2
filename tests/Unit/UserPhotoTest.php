<?php

namespace Tests\Unit;

use App\UserPhoto;
use Tests\TestCase;

class UserPhotoTest extends TestCase
{
    /** @test */
    public function it_creatable()
    {
        UserPhoto::create([
            'user_id' => 1,
            'path' => 'photo.jpg'
        ]);

        $this->assertDatabaseHas('user_photos', [
            'id' => 1,
            'user_id' => 1,
            'path' => 'photo.jpg'
        ]);
    }
}
