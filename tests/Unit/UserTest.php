<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

class UserTest extends TestCase
{
    /** @test */
    public function it_has_photos_relationship()
    {
        $user = factory(User::class)->create();

        $this->assertInstanceOf(HasMany::class, $user->photos());

    }
}
