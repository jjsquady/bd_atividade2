<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('userphotos', 'UserPhotosController@index')->name('userphoto.index');
Route::get('userphotos/upload', 'UserPhotosController@create')->name('userphoto.create');
Route::post('userphotos', 'UserPhotosController@store')->name('userphoto.store');
Route::get('userphotos/{photo}/delete', 'UserPhotosController@destroy')->name('userphoto.delete');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
