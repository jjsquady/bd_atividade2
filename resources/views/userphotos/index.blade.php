@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>User Photos</h1>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('userphoto.create') }}" class="btn btn-success">
                    Upload a photo
                </a>
            </div>
        </div>
        <hr>

        <div class="row">
            @forelse($photos as $key => $photo)
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">#{{ $key }}</div>
                        <div class="panel-body" style="height: 190px;">
                            <img src="/storage/{{ $photo->path }}" width="100%">
                        </div>
                        <div class="panel-footer">
                            <a href="{{ route('userphoto.delete', $photo) }}" class="btn btn-danger">
                                Delete
                            </a>
                        </div>
                    </div>
                </div>
            @empty
                <h3><strong>Not photos uploaded.</strong></h3>
            @endforelse
        </div>

    </div>

@endsection