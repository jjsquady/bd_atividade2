@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Upload a photo:</h1>

        <hr>

        <form action="{{ route('userphoto.store') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <input type="file" name="photo">
                <hr>
                <button type="submit" class="btn btn-primary">
                    Enviar
                </button>
            </div>
        </form>
    </div>

@endsection